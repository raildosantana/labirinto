#include <stdio.h>
#include <stdlib.h>
#include "labirinto.h"

labirinto* leLabirintoDeArquivo(char *nomeArquivo){

    labirinto *lab_in = (labirinto *)malloc(sizeof(labirinto));
    int i , celulas;
    FILE *entrada = fopen(nomeArquivo,"r+");
    if(entrada == NULL){
        printf("LABIRINTO NAO ENCONTRADO\n");
        exit(1);
    }else{
        printf("LABIRINTO ABERTO COM SUCESSO \n");
    }

    fscanf(entrada,"%d %d",&lab_in->linhas,&lab_in->colunas);

    celulas = lab_in->linhas * lab_in->colunas;

    lab_in->mapa = (char *)malloc( sizeof(char) * celulas );

    for(i = 0; i < celulas; i++){
        fscanf(entrada,"%c",lab_in->mapa);
        if(lab_in->mapa[i] == '\n'){
            i--;
        }
    }
    
    fclose(entrada);
    return lab_in;
}


void liberaLabirinto(labirinto *lab){
    free(lab->mapa);
    lab->mapa = NULL;
    free(lab);
    lab = NULL;
}
