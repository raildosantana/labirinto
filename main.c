// Universidade Federal de Minas Gerais - UFMG
// Instituto de Ciências Exatas - ICEX
// Departamento de Ciência da Computação - DCC
// Disciplina : Algoritmo e Estrutura de Dados II = AEDS II
// Trabalho Pratico I - Labirinto
// Autor: Raildo Santana Silva
// Curso: Engenharia de Sistemas

#include <stdio.h>
#include <stdlib.h>
#include "labirinto.h"

int main(int argc, char *argv[]){

    labirinto *labirintoEntrada = leLabirintoDeArquivo(argv[1]);
    printf("DIMENSÕES: %d %d\n",labirintoEntrada->linhas,labirintoEntrada->colunas);
    liberaLabirinto( labirintoEntrada );

    return 0;
}
