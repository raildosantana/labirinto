
#ifndef __LAB_HEADER__
#define __LAB_HEADER__

// Mais funcoes e estruturas podem ser definidas caso voce ache que
// va ajudar o seu trabalho

typedef struct Labirinto {
	// 'i'=inicio, '.'=livre, '1'=parede, 'q'=queijo, 's'=saida
    // mapa[colunas*x + y] Mapa representado por um vetor, nao uma matriz
	char *mapa;
    // numero de linhas
	int linhas;
    // numero de colunas
	int colunas;
} labirinto;

// x seria a linha, j a coluna
typedef struct Posicao {
	int x;
	int y;
} posicao;

typedef struct {
    // numero total de movimentos durante a busca,
    // incluindo movimentos na direcao errada
	int passos;
    // sequencia de posicoes ate a saida
    // este vetor nao precisa conter todo o caminho,
    // mas apenas o caminho da posicao inicial ate a
    // saida (ignore eventuais caminhos errados)
	posicao **posicoes;
    // numero de elementos em **posicoes
    // Este numero sera o numero de passos ate a saida,
    // ignorando os passos em caminhos errados
	int comprimento;
    // numero de moedas encontradas na caminhada total,
    // incluindo aquelas achadas em escolhas erradas de caminho
	int moedasEncontradasNoTotal;
    // numero de moedas encontradas apenas no caminho da posicao
    // inicial ate a saida, ignorando as moedas encontradas em
    // caminhos errados
    int moedasEncontradasNoCaminhoCerto;
    // 0 caso o labirinto nao tenha saida
    // 1 caso ele tenha saida
    // Se o labirinto nao tiver saida, moedasEncontradasNoCaminhoCerto
    // deve ser zero, assim como o vetor de posicoes deve conter apenas
    // a posicao inicial
    int saidaEncontrada;
} percurso;

// Retorna um ponteiro para uma estrutura representando o labirinto
// especificado no arquivo [nome_arquivo]. a primeira linha do arquvo
// [nomeArquivo] deve conter dois inteiros especificando o numero L de
// linhas e o numero C de colunas do labirinto. as proximas L linhas devem
// conter C caracteres representando o labirinto.

labirinto* leLabirintoDeArquivo(char *nomeArquivo);// Implementada

// Cria uma estrutura representando a posicao [x, y]
posicao *criaPosicao(int x, int y);

// Retorna um percurso da posicao inicial [posInicial] ate a saida do labirinto [lab].
// Guarde a posicao inicial no vetor de posicoes na estrutura percurso
percurso* achaSaida(labirinto *lab, posicao *posInicial);

// Cria e retorna a posicao da coordenada inicial (marcada com o caractere 'i')
// no labirinto [lab].
posicao* achaCoordenadaInicial(labirinto *lab);

// Libera memoria do labirinto
void liberaLabirinto(labirinto *lab); // Implementada - UHUUUU 

// Libera a memoria utilizada para representar o percurso
void liberaPercurso(percurso *perc);

// Libera a memoria utilizada para representar a posicao
void liberaPosicao(posicao *pos);

// Imprime o percurso [perc] no arquivo [arquivoSaida]
// como definido na especificacao.
void salvaPercursoEmArquivo(percurso* perc, char* arquivoSaida);

#endif
